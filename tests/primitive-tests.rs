
extern crate nand2tetris;
use nand2tetris::*;

/// |   a   |   b   |  out  |
/// |:-----:|:-----:|:-----:|
/// |   0   |   0   |   1   |
/// |   0   |   1   |   1   |
/// |   1   |   0   |   1   |
/// |   1   |   1   |   0   |
#[test]
fn nand_test() {
    assert_eq!(nand(Zero, Zero), One);
    assert_eq!(nand(Zero, One), One);
    assert_eq!(nand(One, Zero), One);
    assert_eq!(nand(One, One), Zero);
}
