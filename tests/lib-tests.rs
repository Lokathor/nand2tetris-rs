
extern crate nand2tetris;
use nand2tetris::*;

#[test]
fn str16_to_wire16_test() {
    assert_eq!(
        str16_to_wire16("1001100001110110"),
        [One, Zero, Zero, One, One, Zero, Zero, Zero, Zero, One, One, One, Zero, One, One, Zero]
    );
}

#[test]
fn str8_to_wire8_test() {
    assert_eq!(
        str8_to_wire8("00100110"),
        [Zero, Zero, One, Zero, Zero, One, One, Zero]
    );
}

#[test]
fn u16_to_wire16_test() {
    // zero is zero
    assert_eq!(u16_to_wire16(0), [Zero; 16]);

    // one is the final bit only.
    assert_eq!(u16_to_wire16(1), wire16_one);

    // minimum value is the first bit only.
    assert_eq!(u16_to_wire16(std::i16::MIN as u16), wire16_sign_bit);
}

#[test]
fn wire16_to_u16_test() {
    // zero is zero
    assert_eq!(wire16_to_u16([Zero; 16]), 0);

    // one is the final bit only.
    assert_eq!(wire16_to_u16(wire16_one), 1);

    // minimum value is the first bit only.
    assert_eq!(wire16_to_u16(wire16_sign_bit), std::i16::MIN as u16);
}
