
#![allow(non_snake_case)]

extern crate nand2tetris;
use nand2tetris::*;

#[test]
fn bit_test() {
    // Set up our bit for this whole test.
    let mut theBit = Bit::default();

    // Zero to start.
    theBit.tick(Zero, Zero);
    assert_eq!(theBit.get_output(), Zero);

    // We can load a Zero in.
    theBit.tick(Zero, One);
    assert_eq!(theBit.get_output(), Zero);

    // We can NOT load a One in.
    theBit.tick(One, Zero);
    assert_eq!(theBit.get_output(), Zero);

    // We finally load a One up.
    theBit.tick(One, One);
    assert_eq!(theBit.get_output(), One);

    // Zero in without a load bit stays as One output.
    theBit.tick(Zero, Zero);
    assert_eq!(theBit.get_output(), One);

    // As long as the load bit is off, no change.
    theBit.tick(One, Zero);
    assert_eq!(theBit.get_output(), One);

    // Load in a Zero again, just to show off.
    theBit.tick(Zero, One);
    assert_eq!(theBit.get_output(), Zero);

    // Load in One.
    theBit.tick(One, One);
    assert_eq!(theBit.get_output(), One);

    // Cycle once with no change.
    theBit.tick(Zero, Zero);
    assert_eq!(theBit.get_output(), One);

    // Cycle twice with no change.
    theBit.tick(Zero, Zero);
    assert_eq!(theBit.get_output(), One);

    // Cycle a third time with no change.
    theBit.tick(Zero, Zero);
    assert_eq!(theBit.get_output(), One);

    // Clear the bit out.
    theBit.tick(Zero, One);
    assert_eq!(theBit.get_output(), Zero);

    // Don't load anything in one last time.
    theBit.tick(One, Zero);
    assert_eq!(theBit.get_output(), Zero);
}

#[test]
fn register16_test() {
    // Set up our register for this whole test.
    let mut theRegister = Register16::default();

    // We start with a blank register.
    theRegister.tick([Zero; 16], Zero);
    assert_eq!(theRegister.get_output(), [Zero; 16]);

    // We can also load in zero.
    theRegister.tick([Zero; 16], Zero);
    assert_eq!(theRegister.get_output(), [Zero; 16]);

    // We can not load in a value.
    theRegister.tick(u16_to_wire16(-32123i16 as u16), Zero);
    assert_eq!(theRegister.get_output(), [Zero; 16]);

    // Try another.
    theRegister.tick(u16_to_wire16(11111), Zero);
    assert_eq!(theRegister.get_output(), [Zero; 16]);

    // Now load in that value.
    theRegister.tick(u16_to_wire16(-32123i16 as u16), One);
    assert_eq!(theRegister.get_output(), u16_to_wire16(-32123i16 as u16));

    // Load a second time because that's what they did originally.
    theRegister.tick(u16_to_wire16(-32123i16 as u16), One);
    assert_eq!(theRegister.get_output(), u16_to_wire16(-32123i16 as u16));

    // don't-load.
    theRegister.tick(u16_to_wire16(-32123i16 as u16), Zero);
    assert_eq!(theRegister.get_output(), u16_to_wire16(-32123i16 as u16));

    // load.
    theRegister.tick(u16_to_wire16(12345), One);
    assert_eq!(theRegister.get_output(), u16_to_wire16(12345));

    // don't-load, we should see the same as before.
    theRegister.tick(u16_to_wire16(0), Zero);
    assert_eq!(theRegister.get_output(), u16_to_wire16(12345));

    // the original tests went on much longer than this but really you get the
    // idea by now.
}

#[test]
fn ram8x16_test() {
    // Set up our ram for this whole test.
    let mut theRam = Ram8x16::default();

    // theRam.tick(input16, load, address3);

    // Initially don't-load zero.
    theRam.tick([Zero; 16], Zero, [Zero; 3]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load zero.
    theRam.tick([Zero; 16], One, [Zero; 3]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Don't load 11111@0
    theRam.tick(u16_to_wire16(11111), Zero, [Zero; 3]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load 11111@1
    theRam.tick(u16_to_wire16(11111), One, [Zero, Zero, One]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Don't load 11111@0
    theRam.tick(u16_to_wire16(11111), Zero, [Zero; 3]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Don't load 3333@3
    theRam.tick(u16_to_wire16(3333), Zero, [Zero, One, One]);
    assert_eq!(theRam.get_output(), u16_to_wire16(0));

    // Load 3333@3
    theRam.tick(u16_to_wire16(3333), One, [Zero, One, One]);
    assert_eq!(theRam.get_output(), u16_to_wire16(3333));

    // Don't load 11111@1
    theRam.tick(u16_to_wire16(11111), Zero, [Zero, Zero, One]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // We could put more tests in here but we can be pretty confident at this
    // point that it's probably all good. We should really be using quickcheck
    // for this crap, but oh well.
}

#[test]
fn ram64x16_test() {
    // Set up our ram for this whole test.
    let mut theRam = Ram64x16::default();

    // Initially don't-load zero.
    theRam.tick([Zero; 16], Zero, [Zero; 6]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load zero.
    theRam.tick([Zero; 16], One, [Zero; 6]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Don't load 11111@0
    theRam.tick(u16_to_wire16(11111), Zero, [Zero; 6]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load 11111@max
    theRam.tick(u16_to_wire16(11111), One, [One; 6]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Load 3@3
    theRam
        .tick(u16_to_wire16(3), One, [Zero, Zero, Zero, Zero, One, Zero]);
    assert_eq!(theRam.get_output(), u16_to_wire16(3));

    // Read 11111@max
    theRam.tick(u16_to_wire16(11111), Zero, [One; 6]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Read 3@3
    theRam
        .tick(u16_to_wire16(3), Zero, [Zero, Zero, Zero, Zero, One, Zero]);
    assert_eq!(theRam.get_output(), u16_to_wire16(3));
}

#[test]
fn ram512x16_test() {
    // Set up our ram for this whole test.
    let mut theRam = Ram512x16::default();

    // Initially don't-load zero.
    theRam.tick([Zero; 16], Zero, [Zero; 9]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load zero.
    theRam.tick([Zero; 16], One, [Zero; 9]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Don't load 11111@0
    theRam.tick(u16_to_wire16(11111), Zero, [Zero; 9]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load 11111@max
    theRam.tick(u16_to_wire16(11111), One, [One; 9]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Load 3@3
    theRam.tick(
        u16_to_wire16(3),
        One,
        [Zero, Zero, Zero, Zero, Zero, Zero, Zero, One, Zero],
    );
    assert_eq!(theRam.get_output(), u16_to_wire16(3));

    // Read 11111@max
    theRam.tick(u16_to_wire16(11111), Zero, [One; 9]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Read 3@3
    theRam.tick(
        u16_to_wire16(3),
        Zero,
        [Zero, Zero, Zero, Zero, Zero, Zero, Zero, One, Zero],
    );
    assert_eq!(theRam.get_output(), u16_to_wire16(3));
}

#[test]
fn ram4kx16_test() {
    // Set up our ram for this whole test.
    let mut theRam = Ram4kx16::default();

    // Initially don't-load zero.
    theRam.tick([Zero; 16], Zero, [Zero; 12]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load zero.
    theRam.tick([Zero; 16], One, [Zero; 12]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Don't load 11111@0
    theRam.tick(u16_to_wire16(11111), Zero, [Zero; 12]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load 11111@max
    theRam.tick(u16_to_wire16(11111), One, [One; 12]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Load 3@3
    theRam.tick(
        u16_to_wire16(3),
        One,
        [Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, One, Zero],
    );
    assert_eq!(theRam.get_output(), u16_to_wire16(3));

    // Read 11111@max
    theRam.tick(u16_to_wire16(11111), Zero, [One; 12]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Read 3@3
    theRam.tick(
        u16_to_wire16(3),
        Zero,
        [Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, One, Zero],
    );
    assert_eq!(theRam.get_output(), u16_to_wire16(3));
}

#[test]
fn ram16kx16_test() {
    // Set up our ram for this whole test.
    let mut theRam = Ram16kx16::default();

    // Initially don't-load zero.
    theRam.tick([Zero; 16], Zero, [Zero; 14]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load zero.
    theRam.tick([Zero; 16], One, [Zero; 14]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Don't load 11111@0
    theRam.tick(u16_to_wire16(11111), Zero, [Zero; 14]);
    assert_eq!(theRam.get_output(), [Zero; 16]);

    // Load 11111@max
    theRam.tick(u16_to_wire16(11111), One, [One; 14]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Load 3@3
    theRam.tick(
        u16_to_wire16(3),
        One,
        [Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, One, Zero],
    );
    assert_eq!(theRam.get_output(), u16_to_wire16(3));

    // Read 11111@max
    theRam.tick(u16_to_wire16(11111), Zero, [One; 14]);
    assert_eq!(theRam.get_output(), u16_to_wire16(11111));

    // Read 3@3
    theRam.tick(
        u16_to_wire16(3),
        Zero,
        [Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, One, Zero],
    );
    assert_eq!(theRam.get_output(), u16_to_wire16(3));
}

#[test]
fn programCounter16_test() {
    // Set up our counter for this whole test.
    let mut theCounter = ProgramCounter16::default();

    // Do nothing, it should show zero.
    theCounter.tick([Zero; 16], Zero, Zero, Zero);
    assert_eq!(theCounter.get_output(), [Zero; 16]);

    // Inc once, it should show one.
    theCounter.tick([Zero; 16], Zero, Zero, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(1));

    // Inc with a weird input, it should show two.
    theCounter
        .tick(u16_to_wire16(-32123i16 as u16), Zero, Zero, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(2));

    // Load with a weird input, it should show the input.
    theCounter
        .tick(u16_to_wire16(-32123i16 as u16), Zero, One, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(-32123i16 as u16));

    // We can also bump the value again.
    theCounter
        .tick(u16_to_wire16(-32123i16 as u16), Zero, Zero, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(-32122i16 as u16));

    // We can also bump the value again, again.
    theCounter
        .tick(u16_to_wire16(-32123i16 as u16), Zero, Zero, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(-32121i16 as u16));

    // Set the counter to 12345.
    theCounter.tick(u16_to_wire16(12345), Zero, One, Zero);
    assert_eq!(theCounter.get_output(), u16_to_wire16(12345));

    // Zero the counter.
    theCounter.tick(u16_to_wire16(12345), One, One, Zero);
    assert_eq!(theCounter.get_output(), u16_to_wire16(0));

    // Load back the counter.
    theCounter.tick(u16_to_wire16(12345), Zero, One, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(12345));

    // Zero the counter again.
    theCounter.tick(u16_to_wire16(12345), One, One, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(0));

    // Bump it.
    theCounter.tick(u16_to_wire16(12345), Zero, Zero, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(1));

    // Zero it a slightly new way.
    theCounter.tick(u16_to_wire16(12345), One, Zero, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(0));

    // Load a zero instead of a reset.
    theCounter.tick(u16_to_wire16(0), Zero, One, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(0));

    // Bump it.
    theCounter.tick(u16_to_wire16(0), Zero, Zero, One);
    assert_eq!(theCounter.get_output(), u16_to_wire16(1));

    // Reset.
    theCounter.tick(u16_to_wire16(22222), One, Zero, Zero);
    assert_eq!(theCounter.get_output(), u16_to_wire16(0));
}
