
extern crate nand2tetris;
use nand2tetris::*;

/// |   a   |   b   |  sum  | carry |
/// |:-----:|:-----:|:-----:|:-----:|
/// |   0   |   0   |   0   |   0   |
/// |   0   |   1   |   1   |   0   |
/// |   1   |   0   |   1   |   0   |
/// |   1   |   1   |   0   |   1   |
#[test]
fn halfadder_test() {
    assert_eq!(halfadder(Zero, Zero), (Zero, Zero));
    assert_eq!(halfadder(Zero, One), (One, Zero));
    assert_eq!(halfadder(One, Zero), (One, Zero));
    assert_eq!(halfadder(One, One), (Zero, One));
}

/// |   a   |   b   |   c   |  sum  | carry |
/// |:-----:|:-----:|:-----:|:-----:|:-----:|
/// |   0   |   0   |   0   |   0   |   0   |
/// |   0   |   0   |   1   |   1   |   0   |
/// |   0   |   1   |   0   |   1   |   0   |
/// |   0   |   1   |   1   |   0   |   1   |
/// |   1   |   0   |   0   |   1   |   0   |
/// |   1   |   0   |   1   |   0   |   1   |
/// |   1   |   1   |   0   |   0   |   1   |
/// |   1   |   1   |   1   |   1   |   1   |
#[test]
fn fulladder_test() {
    assert_eq!(fulladder(Zero, Zero, Zero), (Zero, Zero));
    assert_eq!(fulladder(Zero, Zero, One), (One, Zero));
    assert_eq!(fulladder(Zero, One, Zero), (One, Zero));
    assert_eq!(fulladder(Zero, One, One), (Zero, One));
    assert_eq!(fulladder(One, Zero, Zero), (One, Zero));
    assert_eq!(fulladder(One, Zero, One), (Zero, One));
    assert_eq!(fulladder(One, One, Zero), (Zero, One));
    assert_eq!(fulladder(One, One, One), (One, One));
}

/// |        a         |        b         |       out        |
/// |:----------------:|:----------------:|:----------------:|
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 |
/// | 0000000000000000 | 1111111111111111 | 1111111111111111 |
/// | 1111111111111111 | 1111111111111111 | 1111111111111110 |
/// | 1010101010101010 | 0101010101010101 | 1111111111111111 |
/// | 0011110011000011 | 0000111111110000 | 0100110010110011 |
/// | 0001001000110100 | 1001100001110110 | 1010101010101010 |
#[test]
fn add16_test() {
    let a = [Zero; 16];
    let b = [Zero; 16];
    assert_eq!(add16(a, b), [Zero; 16]);

    let a = [Zero; 16];
    let b = [One; 16];
    assert_eq!(add16(a, b), [One; 16]);

    let a = [One; 16];
    let b = [One; 16];
    assert_eq!(add16(a, b), str16_to_wire16("1111111111111110"));

    let a = str16_to_wire16("1010101010101010");
    let b = str16_to_wire16("0101010101010101");
    assert_eq!(add16(a, b), str16_to_wire16("1111111111111111"));

    let a = str16_to_wire16("0011110011000011");
    let b = str16_to_wire16("0000111111110000");
    assert_eq!(add16(a, b), str16_to_wire16("0100110010110011"));

    let a = str16_to_wire16("0001001000110100");
    let b = str16_to_wire16("1001100001110110");
    assert_eq!(add16(a, b), str16_to_wire16("1010101010101010"));
}

/// |        in        |       out        |
/// |:----------------:|:----------------:|
/// | 0000000000000000 | 0000000000000001 |
/// | 1111111111111111 | 0000000000000000 |
/// | 0000000000000101 | 0000000000000110 |
/// | 1111111111111011 | 1111111111111100 |
#[test]
fn inc16_test() {
    let input = str16_to_wire16("0000000000000000");
    assert_eq!(inc16(input), str16_to_wire16("0000000000000001"));

    let input = str16_to_wire16("1111111111111111");
    assert_eq!(inc16(input), str16_to_wire16("0000000000000000"));

    let input = str16_to_wire16("0000000000000101");
    assert_eq!(inc16(input), str16_to_wire16("0000000000000110"));

    let input = str16_to_wire16("1111111111111011");
    assert_eq!(inc16(input), str16_to_wire16("1111111111111100"));
}

/// |        x         |        y         |zx |nx |zy |ny | f |no |       out        |zr |ng |
/// |:----------------:|:----------------:|:-:|:-:|:-:|:-:|:-:|:-:|:----------------:|:-:|:-:|
/// | 0000000000000000 | 1111111111111111 | 1 | 0 | 1 | 0 | 1 | 0 | 0000000000000000 | 1 | 0 |
/// | 0000000000000000 | 1111111111111111 | 1 | 1 | 1 | 1 | 1 | 1 | 0000000000000001 | 0 | 0 |
/// | 0000000000000000 | 1111111111111111 | 1 | 1 | 1 | 0 | 1 | 0 | 1111111111111111 | 0 | 1 |
/// | 0000000000000000 | 1111111111111111 | 0 | 0 | 1 | 1 | 0 | 0 | 0000000000000000 | 1 | 0 |
/// | 0000000000000000 | 1111111111111111 | 1 | 1 | 0 | 0 | 0 | 0 | 1111111111111111 | 0 | 1 |
/// | 0000000000000000 | 1111111111111111 | 0 | 0 | 1 | 1 | 0 | 1 | 1111111111111111 | 0 | 1 |
/// | 0000000000000000 | 1111111111111111 | 1 | 1 | 0 | 0 | 0 | 1 | 0000000000000000 | 1 | 0 |
/// | 0000000000000000 | 1111111111111111 | 0 | 0 | 1 | 1 | 1 | 1 | 0000000000000000 | 1 | 0 |
/// | 0000000000000000 | 1111111111111111 | 1 | 1 | 0 | 0 | 1 | 1 | 0000000000000001 | 0 | 0 |
/// | 0000000000000000 | 1111111111111111 | 0 | 1 | 1 | 1 | 1 | 1 | 0000000000000001 | 0 | 0 |
/// | 0000000000000000 | 1111111111111111 | 1 | 1 | 0 | 1 | 1 | 1 | 0000000000000000 | 1 | 0 |
/// | 0000000000000000 | 1111111111111111 | 0 | 0 | 1 | 1 | 1 | 0 | 1111111111111111 | 0 | 1 |
/// | 0000000000000000 | 1111111111111111 | 1 | 1 | 0 | 0 | 1 | 0 | 1111111111111110 | 0 | 1 |
/// | 0000000000000000 | 1111111111111111 | 0 | 0 | 0 | 0 | 1 | 0 | 1111111111111111 | 0 | 1 |
/// | 0000000000000000 | 1111111111111111 | 0 | 1 | 0 | 0 | 1 | 1 | 0000000000000001 | 0 | 0 |
/// | 0000000000000000 | 1111111111111111 | 0 | 0 | 0 | 1 | 1 | 1 | 1111111111111111 | 0 | 1 |
/// | 0000000000000000 | 1111111111111111 | 0 | 0 | 0 | 0 | 0 | 0 | 0000000000000000 | 1 | 0 |
/// | 0000000000000000 | 1111111111111111 | 0 | 1 | 0 | 1 | 0 | 1 | 1111111111111111 | 0 | 1 |
/// | 0000000000010001 | 0000000000000011 | 1 | 0 | 1 | 0 | 1 | 0 | 0000000000000000 | 1 | 0 |
/// | 0000000000010001 | 0000000000000011 | 1 | 1 | 1 | 1 | 1 | 1 | 0000000000000001 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 1 | 1 | 1 | 0 | 1 | 0 | 1111111111111111 | 0 | 1 |
/// | 0000000000010001 | 0000000000000011 | 0 | 0 | 1 | 1 | 0 | 0 | 0000000000010001 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 1 | 1 | 0 | 0 | 0 | 0 | 0000000000000011 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 0 | 0 | 1 | 1 | 0 | 1 | 1111111111101110 | 0 | 1 |
/// | 0000000000010001 | 0000000000000011 | 1 | 1 | 0 | 0 | 0 | 1 | 1111111111111100 | 0 | 1 |
/// | 0000000000010001 | 0000000000000011 | 0 | 0 | 1 | 1 | 1 | 1 | 1111111111101111 | 0 | 1 |
/// | 0000000000010001 | 0000000000000011 | 1 | 1 | 0 | 0 | 1 | 1 | 1111111111111101 | 0 | 1 |
/// | 0000000000010001 | 0000000000000011 | 0 | 1 | 1 | 1 | 1 | 1 | 0000000000010010 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 1 | 1 | 0 | 1 | 1 | 1 | 0000000000000100 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 0 | 0 | 1 | 1 | 1 | 0 | 0000000000010000 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 1 | 1 | 0 | 0 | 1 | 0 | 0000000000000010 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 0 | 0 | 0 | 0 | 1 | 0 | 0000000000010100 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 0 | 1 | 0 | 0 | 1 | 1 | 0000000000001110 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 0 | 0 | 0 | 1 | 1 | 1 | 1111111111110010 | 0 | 1 |
/// | 0000000000010001 | 0000000000000011 | 0 | 0 | 0 | 0 | 0 | 0 | 0000000000000001 | 0 | 0 |
/// | 0000000000010001 | 0000000000000011 | 0 | 1 | 0 | 1 | 0 | 1 | 0000000000010011 | 0 | 0 |
#[test]
fn alu_test() {
    let x = [Zero; 16];
    let y = [One; 16];

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 0 | 1 | 0 | 1 | 0 | 0000000000000000 | 1 | 0 |
    let (out, zr, ng) = alu(x, y, One, Zero, One, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("0000000000000000"));
    assert_eq!(zr, One);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 1 | 1 | 1 | 1 | 0000000000000001 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, One, One, One, One);
    assert_eq!(out, str16_to_wire16("0000000000000001"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 1 | 0 | 1 | 0 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, One, One, One, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 0 | 0 | 0000000000000000 | 1 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, Zero, Zero);
    assert_eq!(out, str16_to_wire16("0000000000000000"));
    assert_eq!(zr, One);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 0 | 0 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, Zero, Zero);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 0 | 1 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, Zero, One);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 0 | 1 | 0000000000000000 | 1 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, Zero, One);
    assert_eq!(out, str16_to_wire16("0000000000000000"));
    assert_eq!(zr, One);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 1 | 1 | 0000000000000000 | 1 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, One, One);
    assert_eq!(out, str16_to_wire16("0000000000000000"));
    assert_eq!(zr, One);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 1 | 1 | 0000000000000001 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, One, One);
    assert_eq!(out, str16_to_wire16("0000000000000001"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 1 | 1 | 1 | 1 | 1 | 0000000000000001 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, One, One, One, One, One);
    assert_eq!(out, str16_to_wire16("0000000000000001"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 1 | 1 | 1 | 0000000000000000 | 1 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, One, One, One);
    assert_eq!(out, str16_to_wire16("0000000000000000"));
    assert_eq!(zr, One);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 1 | 0 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, One, Zero);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 1 | 0 | 1111111111111110 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("1111111111111110"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 0 | 0 | 1 | 0 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, Zero, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 0 | 1 | 1 | 1 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, Zero, One, One, One);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 1 | 0 | 0000000000000001 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("1111111111111110"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 0 | 0 | 0 | 0 | 0000000000000000 | 1 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, Zero, Zero, Zero, Zero);
    assert_eq!(out, str16_to_wire16("0000000000000000"));
    assert_eq!(zr, One);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 1 | 0 | 1 | 0 | 1 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, One, Zero, One, Zero, One);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    let x = str16_to_wire16("0000000000010001");
    let y = str16_to_wire16("0000000000000011");

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 0 | 1 | 0 | 1 | 0 | 0000000000000000 | 1 | 0 |
    let (out, zr, ng) = alu(x, y, One, Zero, One, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("0000000000000000"));
    assert_eq!(zr, One);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 1 | 1 | 1 | 1 | 0000000000000001 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, One, One, One, One);
    assert_eq!(out, str16_to_wire16("0000000000000001"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 1 | 0 | 1 | 0 | 1111111111111111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, One, One, One, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("1111111111111111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 0 | 0 | 0000000000010001 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, Zero, Zero);
    assert_eq!(out, str16_to_wire16("0000000000010001"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 0 | 0 | 0000000000000011 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, Zero, Zero);
    assert_eq!(out, str16_to_wire16("0000000000000011"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 0 | 1 | 1111111111101110 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, Zero, One);
    assert_eq!(out, str16_to_wire16("1111111111101110"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 0 | 1 | 1111111111111100 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, Zero, One);
    assert_eq!(out, str16_to_wire16("1111111111111100"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 1 | 1 | 1111111111101111 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, One, One);
    assert_eq!(out, str16_to_wire16("1111111111101111"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 1 | 1 | 1111111111111101 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, One, One);
    assert_eq!(out, str16_to_wire16("1111111111111101"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 1 | 1 | 1 | 1 | 1 | 0000000000010010 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, One, One, One, One, One);
    assert_eq!(out, str16_to_wire16("0000000000010010"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 1 | 1 | 1 | 0000000000000100 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, One, One, One);
    assert_eq!(out, str16_to_wire16("0000000000000100"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 1 | 1 | 1 | 0 | 0000000000010000 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, One, One, One, Zero);
    assert_eq!(out, str16_to_wire16("0000000000010000"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 1 | 1 | 0 | 0 | 1 | 0 | 0000000000000010 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, One, One, Zero, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("0000000000000010"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 0 | 0 | 1 | 0 | 0000000000010100 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, Zero, Zero, One, Zero);
    assert_eq!(out, str16_to_wire16("0000000000010100"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 1 | 0 | 0 | 1 | 1 | 0000000000001110 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, One, Zero, Zero, One, One);
    assert_eq!(out, str16_to_wire16("0000000000001110"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 0 | 1 | 1 | 1 | 1111111111110010 | 0 | 1 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, Zero, One, One, One);
    assert_eq!(out, str16_to_wire16("1111111111110010"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, One);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 0 | 0 | 0 | 0 | 0 | 0000000000000001 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, Zero, Zero, Zero, Zero, Zero);
    assert_eq!(out, str16_to_wire16("0000000000000001"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 1 | 0 | 1 | 0 | 1 | 0000000000010011 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, One, Zero, One, Zero, One);
    assert_eq!(out, str16_to_wire16("0000000000010011"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);

    //|zx |nx |zy |ny | f |no |       out        |zr |ng |
    //| 0 | 1 | 0 | 1 | 0 | 1 | 0000000000010011 | 0 | 0 |
    let (out, zr, ng) = alu(x, y, Zero, One, Zero, One, Zero, One);
    assert_eq!(out, str16_to_wire16("0000000000010011"));
    assert_eq!(zr, Zero);
    assert_eq!(ng, Zero);
}
