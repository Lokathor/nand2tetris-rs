extern crate nand2tetris;
use nand2tetris::*;

#[test]
fn add_test() {
    // This program will add 2 and 3 using the registers, then place the result
    // into ram[0].
    let mut vm = HackVM::new(vec![
        0b0000000000000010, // A set 2
        0b1110110000010000, // A store D
        0b0000000000000011, // A set 3
        0b1110000010010000, // A + D store D
        0b0000000000000000, // A set 0
        0b1110001100001000, // D store M
    ]);
    assert_eq!(vm.get_a_register(), 0);
    assert_eq!(vm.get_d_register(), 0);
    assert_eq!(vm.get_program_counter(), 0);
    assert_eq!(vm.get_ram()[0], 0);
    vm.tick();
    assert_eq!(vm.get_a_register(), 2);
    assert_eq!(vm.get_d_register(), 0);
    assert_eq!(vm.get_program_counter(), 1);
    assert_eq!(vm.get_ram()[0], 0);
    vm.tick();
    assert_eq!(vm.get_a_register(), 2);
    assert_eq!(vm.get_d_register(), 2);
    assert_eq!(vm.get_program_counter(), 2);
    assert_eq!(vm.get_ram()[0], 0);
    vm.tick();
    assert_eq!(vm.get_a_register(), 3);
    assert_eq!(vm.get_d_register(), 2);
    assert_eq!(vm.get_program_counter(), 3);
    assert_eq!(vm.get_ram()[0], 0);
    vm.tick();
    assert_eq!(vm.get_a_register(), 3);
    assert_eq!(vm.get_d_register(), 5);
    assert_eq!(vm.get_program_counter(), 4);
    assert_eq!(vm.get_ram()[0], 0);
    vm.tick();
    assert_eq!(vm.get_a_register(), 0);
    assert_eq!(vm.get_d_register(), 5);
    assert_eq!(vm.get_program_counter(), 5);
    assert_eq!(vm.get_ram()[0], 0);
    vm.tick();
    assert_eq!(vm.get_a_register(), 0);
    assert_eq!(vm.get_d_register(), 5);
    assert_eq!(vm.get_program_counter(), 6);
    assert_eq!(vm.get_ram()[0], 5);
}

#[test]
fn max_test() {
    // Computes the maximum of R0 and R1, then writes the result to R2.
    let _vm = HackVM::new(vec![
        // TODO
        0b0000000000000000, // A set 0
        0b1111110000010000,
        0b0000000000000001, // A set 1
        0b1111010011010000,
        0b0000000000001010, // A set 10
        0b1110001100000001,
        0b0000000000000001, // A set 1
        0b1111110000010000,
        0b0000000000001100, // A set 12
        0b1110101010000111,
        0b0000000000000000, // A set 0
        0b1111110000010000,
        0b0000000000000010, // A set 2
        0b1110001100001000,
        0b0000000000001110, // A set 14
        0b1110101010000111,
    ]);
}
