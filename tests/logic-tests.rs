
extern crate nand2tetris;
use nand2tetris::*;

/// |  in   |  out  |
/// |:-----:|:-----:|
/// |   0   |   1   |
/// |   1   |   0   |
#[test]
fn not_test() {
    assert_eq!(not(Zero), One);
    assert_eq!(not(One), Zero);
}

/// |   a   |   b   |  out  |
/// |:-----:|:-----:|:-----:|
/// |   0   |   0   |   0   |
/// |   0   |   1   |   0   |
/// |   1   |   0   |   0   |
/// |   1   |   1   |   1   |
#[test]
fn and_test() {
    assert_eq!(and(Zero, Zero), Zero);
    assert_eq!(and(Zero, One), Zero);
    assert_eq!(and(One, Zero), Zero);
    assert_eq!(and(One, One), One);
}

/// |   a   |   b   |  out  |
/// |:-----:|:-----:|:-----:|
/// |   0   |   0   |   0   |
/// |   0   |   1   |   1   |
/// |   1   |   0   |   1   |
/// |   1   |   1   |   1   |
#[test]
fn or_test() {
    assert_eq!(or(Zero, Zero), Zero);
    assert_eq!(or(Zero, One), One);
    assert_eq!(or(One, Zero), One);
    assert_eq!(or(One, One), One);
}

/// |   a   |   b   |  out  |
/// |:-----:|:-----:|:-----:|
/// |   0   |   0   |   0   |
/// |   0   |   1   |   1   |
/// |   1   |   0   |   1   |
/// |   1   |   1   |   0   |
#[test]
fn xor_test() {
    assert_eq!(xor(Zero, Zero), Zero);
    assert_eq!(xor(Zero, One), One);
    assert_eq!(xor(One, Zero), One);
    assert_eq!(xor(One, One), Zero);
}

/// |   a   |   b   |  sel  |  out  |
/// |:-----:|:-----:|:-----:|:------|
/// |   0   |   0   |   0   |   0   |
/// |   0   |   0   |   1   |   0   |
/// |   0   |   1   |   0   |   0   |
/// |   0   |   1   |   1   |   1   |
/// |   1   |   0   |   0   |   1   |
/// |   1   |   0   |   1   |   0   |
/// |   1   |   1   |   0   |   1   |
/// |   1   |   1   |   1   |   1   |
#[test]
fn mux_test() {
    assert_eq!(mux(Zero, Zero, Zero), Zero);
    assert_eq!(mux(Zero, Zero, One), Zero);
    assert_eq!(mux(Zero, One, Zero), Zero);
    assert_eq!(mux(Zero, One, One), One);
    assert_eq!(mux(One, Zero, Zero), One);
    assert_eq!(mux(One, Zero, One), Zero);
    assert_eq!(mux(One, One, Zero), One);
    assert_eq!(mux(One, One, One), One);
}

/// |  in   |  sel  |   a   |   b   |
/// |:-----:|:-----:|:-----:|:------|
/// |   0   |   0   |   0   |   0   |
/// |   0   |   1   |   0   |   0   |
/// |   1   |   0   |   1   |   0   |
/// |   1   |   1   |   0   |   1   |
#[test]
fn dmux_test() {
    assert_eq!(dmux(Zero, Zero), (Zero, Zero));
    assert_eq!(dmux(Zero, One), (Zero, Zero));
    assert_eq!(dmux(One, Zero), (One, Zero));
    assert_eq!(dmux(One, One), (Zero, One));
}

/// |        in        |       out        |
/// |:----------------:|:----------------:|
/// | 0000000000000000 | 1111111111111111 |
/// | 1111111111111111 | 0000000000000000 |
/// | 1010101010101010 | 0101010101010101 |
/// | 0011110011000011 | 1100001100111100 |
/// | 0001001000110100 | 1110110111001011 |
#[test]
fn not16_test() {
    assert_eq!(not16([Zero; 16]), [One; 16]);
    assert_eq!(not16([One; 16]), [Zero; 16]);
    assert_eq!(
        not16(str16_to_wire16("1010101010101010")),
        str16_to_wire16("0101010101010101")
    );
    assert_eq!(
        not16(str16_to_wire16("0011110011000011")),
        str16_to_wire16("1100001100111100")
    );
    assert_eq!(
        not16(str16_to_wire16("0001001000110100")),
        str16_to_wire16("1110110111001011")
    );
}

/// |        a         |        b         |       out        |
/// |:----------------:|:----------------:|:----------------:|
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 |
/// | 0000000000000000 | 1111111111111111 | 0000000000000000 |
/// | 1111111111111111 | 1111111111111111 | 1111111111111111 |
/// | 1010101010101010 | 0101010101010101 | 0000000000000000 |
/// | 0011110011000011 | 0000111111110000 | 0000110011000000 |
/// | 0001001000110100 | 1001100001110110 | 0001000000110100 |
#[test]
fn and16_test() {
    assert_eq!(and16([Zero; 16], [Zero; 16]), [Zero; 16]);
    assert_eq!(and16([Zero; 16], [One; 16]), [Zero; 16]);
    assert_eq!(and16([One; 16], [One; 16]), [One; 16]);
    assert_eq!(
        and16(
            str16_to_wire16("1010101010101010"),
            str16_to_wire16("0101010101010101"),
        ),
        str16_to_wire16("0000000000000000")
    );
    assert_eq!(
        and16(
            str16_to_wire16("0011110011000011"),
            str16_to_wire16("0000111111110000"),
        ),
        str16_to_wire16("0000110011000000")
    );
    assert_eq!(
        and16(
            str16_to_wire16("0001001000110100"),
            str16_to_wire16("1001100001110110"),
        ),
        str16_to_wire16("0001000000110100")
    );
}

/// |        a         |        b         |       out        |
/// |:----------------:|:----------------:|:----------------:|
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 |
/// | 0000000000000000 | 1111111111111111 | 1111111111111111 |
/// | 1111111111111111 | 1111111111111111 | 1111111111111111 |
/// | 1010101010101010 | 0101010101010101 | 1111111111111111 |
/// | 0011110011000011 | 0000111111110000 | 0011111111110011 |
/// | 0001001000110100 | 1001100001110110 | 1001101001110110 |
#[test]
fn or16_test() {
    assert_eq!(or16([Zero; 16], [Zero; 16]), [Zero; 16]);
    assert_eq!(or16([Zero; 16], [One; 16]), [One; 16]);
    assert_eq!(or16([One; 16], [One; 16]), [One; 16]);
    assert_eq!(
        or16(
            str16_to_wire16("1010101010101010"),
            str16_to_wire16("0101010101010101"),
        ),
        str16_to_wire16("1111111111111111")
    );
    assert_eq!(
        or16(
            str16_to_wire16("0011110011000011"),
            str16_to_wire16("0000111111110000"),
        ),
        str16_to_wire16("0011111111110011")
    );
    assert_eq!(
        or16(
            str16_to_wire16("0001001000110100"),
            str16_to_wire16("1001100001110110"),
        ),
        str16_to_wire16("1001101001110110")
    );
}

/// |        a         |        b         | sel |       out        |
/// |:----------------:|:----------------:|:---:|:----------------:|
/// | 0000000000000000 | 0000000000000000 |  0  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 |  1  | 0000000000000000 |
/// | 0000000000000000 | 0001001000110100 |  0  | 0000000000000000 |
/// | 0000000000000000 | 0001001000110100 |  1  | 0001001000110100 |
/// | 1001100001110110 | 0000000000000000 |  0  | 1001100001110110 |
/// | 1001100001110110 | 0000000000000000 |  1  | 0000000000000000 |
/// | 1010101010101010 | 0101010101010101 |  0  | 1010101010101010 |
/// | 1010101010101010 | 0101010101010101 |  1  | 0101010101010101 |
#[test]
fn mux16_test() {
    let a = str16_to_wire16("0000000000000000");
    let b = str16_to_wire16("0000000000000000");
    assert_eq!(mux16(a, b, Zero), a);
    assert_eq!(mux16(a, b, One), b);

    let a = str16_to_wire16("0000000000000000");
    let b = str16_to_wire16("0001001000110100");
    assert_eq!(mux16(a, b, Zero), a);
    assert_eq!(mux16(a, b, One), b);

    let a = str16_to_wire16("1001100001110110");
    let b = str16_to_wire16("0000000000000000");
    assert_eq!(mux16(a, b, Zero), a);
    assert_eq!(mux16(a, b, One), b);

    let a = str16_to_wire16("1010101010101010");
    let b = str16_to_wire16("0101010101010101");
    assert_eq!(mux16(a, b, Zero), a);
    assert_eq!(mux16(a, b, One), b);
}

/// |     in     | out |
/// |:----------:|:---:|
/// |  00000000  |  0  |
/// |  11111111  |  1  |
/// |  00010000  |  1  |
/// |  00000001  |  1  |
/// |  00100110  |  1  |
#[test]
fn or8way_test() {
    assert_eq!(or8way([Zero; 8]), Zero);
    assert_eq!(or8way([One; 8]), One);
    assert_eq!(or8way(str8_to_wire8("00010000")), One);
    assert_eq!(or8way(str8_to_wire8("00000001")), One);
    assert_eq!(or8way(str8_to_wire8("00100110")), One);
}

/// |        a         |        b         |        c         |        d         | sel  |       out        |
/// |:----------------:|:----------------:|:----------------:|:----------------:|:----:|:----------------:|
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  00  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  01  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  10  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  11  | 0000000000000000 |
/// | 0001001000110100 | 1001100001110110 | 1010101010101010 | 0101010101010101 |  00  | 0001001000110100 | a
/// | 0001001000110100 | 1001100001110110 | 1010101010101010 | 0101010101010101 |  01  | 1001100001110110 | b
/// | 0001001000110100 | 1001100001110110 | 1010101010101010 | 0101010101010101 |  10  | 1010101010101010 | c
/// | 0001001000110100 | 1001100001110110 | 1010101010101010 | 0101010101010101 |  11  | 0101010101010101 | d
#[test]
fn mux4way16_test() {
    let a = [Zero; 16];
    let b = [Zero; 16];
    let c = [Zero; 16];
    let d = [Zero; 16];
    assert_eq!(mux4way16(a, b, c, d, [Zero, Zero]), a);
    assert_eq!(mux4way16(a, b, c, d, [Zero, One]), b);
    assert_eq!(mux4way16(a, b, c, d, [One, Zero]), c);
    assert_eq!(mux4way16(a, b, c, d, [One, One]), d);

    let a = str16_to_wire16("0001001000110100");
    let b = str16_to_wire16("1001100001110110");
    let c = str16_to_wire16("1010101010101010");
    let d = str16_to_wire16("0101010101010101");
    assert_eq!(mux4way16(a, b, c, d, [Zero, Zero]), a);
    assert_eq!(mux4way16(a, b, c, d, [Zero, One]), b);
    assert_eq!(mux4way16(a, b, c, d, [One, Zero]), c);
    assert_eq!(mux4way16(a, b, c, d, [One, One]), d);
}

/// You can't even read flippin read this nonsense :(
///
/// |        a         |        b         |        c         |        d         |        e         |        f         |        g         |        h         |  sel  |       out        |
/// |:----------------:|:----------------:|:----------------:|:----------------:|:----------------:|:----------------:|:----------------:|:----------------:|:-----:|:----------------:|
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  000  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  001  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  010  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  011  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  100  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  101  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  110  | 0000000000000000 |
/// | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 | 0000000000000000 |  111  | 0000000000000000 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  000  | 0001001000110100 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  001  | 0010001101000101 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  010  | 0011010001010110 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  011  | 0100010101100111 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  100  | 0101011001111000 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  101  | 0110011110001001 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  110  | 0111100010011010 |
/// | 0001001000110100 | 0010001101000101 | 0011010001010110 | 0100010101100111 | 0101011001111000 | 0110011110001001 | 0111100010011010 | 1000100110101011 |  111  | 1000100110101011 |
#[test]
fn mux8way16_test() {
    let a = [Zero; 16];
    let b = [Zero; 16];
    let c = [Zero; 16];
    let d = [Zero; 16];
    let e = [Zero; 16];
    let f = [Zero; 16];
    let g = [Zero; 16];
    let h = [Zero; 16];
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, Zero, Zero]), a);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, Zero, One]), b);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, One, Zero]), c);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, One, One]), d);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, Zero, Zero]), e);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, Zero, One]), f);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, One, Zero]), g);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, One, One]), h);

    let a = str16_to_wire16("0001001000110100");
    let b = str16_to_wire16("0010001101000101");
    let c = str16_to_wire16("0011010001010110");
    let d = str16_to_wire16("0100010101100111");
    let e = str16_to_wire16("0101011001111000");
    let f = str16_to_wire16("0110011110001001");
    let g = str16_to_wire16("0111100010011010");
    let h = str16_to_wire16("1000100110101011");
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, Zero, Zero]), a);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, Zero, One]), b);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, One, Zero]), c);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [Zero, One, One]), d);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, Zero, Zero]), e);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, Zero, One]), f);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, One, Zero]), g);
    assert_eq!(mux8way16(a, b, c, d, e, f, g, h, [One, One, One]), h);
}

/// | in  | sel  |  a  |  b  |  c  |  d  |
/// |:---:|:----:|:---:|:---:|:---:|:---:|
/// |  0  |  00  |  0  |  0  |  0  |  0  |
/// |  0  |  01  |  0  |  0  |  0  |  0  |
/// |  0  |  10  |  0  |  0  |  0  |  0  |
/// |  0  |  11  |  0  |  0  |  0  |  0  |
/// |  1  |  00  |  1  |  0  |  0  |  0  |
/// |  1  |  01  |  0  |  1  |  0  |  0  |
/// |  1  |  10  |  0  |  0  |  1  |  0  |
/// |  1  |  11  |  0  |  0  |  0  |  1  |
#[test]
fn dmux4way_test() {
    assert_eq!(dmux4way(Zero, [Zero, Zero]), (Zero, Zero, Zero, Zero));
    assert_eq!(dmux4way(Zero, [Zero, One]), (Zero, Zero, Zero, Zero));
    assert_eq!(dmux4way(Zero, [One, Zero]), (Zero, Zero, Zero, Zero));
    assert_eq!(dmux4way(Zero, [One, One]), (Zero, Zero, Zero, Zero));
    assert_eq!(dmux4way(One, [Zero, Zero]), (One, Zero, Zero, Zero));
    assert_eq!(dmux4way(One, [Zero, One]), (Zero, One, Zero, Zero));
    assert_eq!(dmux4way(One, [One, Zero]), (Zero, Zero, One, Zero));
    assert_eq!(dmux4way(One, [One, One]), (Zero, Zero, Zero, One));
}

/// | in  |  sel  |  a  |  b  |  c  |  d  |  e  |  f  |  g  |  h  |
/// |:---:|:-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
/// |  0  |  000  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  0  |  001  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  0  |  010  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  0  |  011  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  0  |  100  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  0  |  101  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  0  |  110  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  0  |  111  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  1  |  000  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  1  |  001  |  0  |  1  |  0  |  0  |  0  |  0  |  0  |  0  |
/// |  1  |  010  |  0  |  0  |  1  |  0  |  0  |  0  |  0  |  0  |
/// |  1  |  011  |  0  |  0  |  0  |  1  |  0  |  0  |  0  |  0  |
/// |  1  |  100  |  0  |  0  |  0  |  0  |  1  |  0  |  0  |  0  |
/// |  1  |  101  |  0  |  0  |  0  |  0  |  0  |  1  |  0  |  0  |
/// |  1  |  110  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |  0  |
/// |  1  |  111  |  0  |  0  |  0  |  0  |  0  |  0  |  0  |  1  |
#[test]
fn dmux8way_test() {
    let out = (Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero);
    assert_eq!(dmux8way(Zero, [Zero, Zero, Zero]), out);
    assert_eq!(dmux8way(Zero, [Zero, Zero, One]), out);
    assert_eq!(dmux8way(Zero, [Zero, One, Zero]), out);
    assert_eq!(dmux8way(Zero, [Zero, One, One]), out);
    assert_eq!(dmux8way(Zero, [One, Zero, Zero]), out);
    assert_eq!(dmux8way(Zero, [One, Zero, One]), out);
    assert_eq!(dmux8way(Zero, [One, One, Zero]), out);
    assert_eq!(dmux8way(Zero, [One, One, One]), out);

    let out = (One, Zero, Zero, Zero, Zero, Zero, Zero, Zero);
    assert_eq!(dmux8way(One, [Zero, Zero, Zero]), out);

    let out = (Zero, One, Zero, Zero, Zero, Zero, Zero, Zero);
    assert_eq!(dmux8way(One, [Zero, Zero, One]), out);

    let out = (Zero, Zero, One, Zero, Zero, Zero, Zero, Zero);
    assert_eq!(dmux8way(One, [Zero, One, Zero]), out);

    let out = (Zero, Zero, Zero, One, Zero, Zero, Zero, Zero);
    assert_eq!(dmux8way(One, [Zero, One, One]), out);

    let out = (Zero, Zero, Zero, Zero, One, Zero, Zero, Zero);
    assert_eq!(dmux8way(One, [One, Zero, Zero]), out);

    let out = (Zero, Zero, Zero, Zero, Zero, One, Zero, Zero);
    assert_eq!(dmux8way(One, [One, Zero, One]), out);

    let out = (Zero, Zero, Zero, Zero, Zero, Zero, One, Zero);
    assert_eq!(dmux8way(One, [One, One, Zero]), out);

    let out = (Zero, Zero, Zero, Zero, Zero, Zero, Zero, One);
    assert_eq!(dmux8way(One, [One, One, One]), out);
}
