//! Code for chips that carry a value across time.
//!
//! The basic unit of data storage over time in the simulation is the
//! `DataFlipFlop` type from the `primitives` module. Every step of the
//! simulation you can `tick` it with an input value, and thereafter it will
//! return that input value as often as you like with `get_output`. All the
//! chips of this module follow a similar general protocol, where you call
//! `tick` with the input setting for that timestep, and then you can repeatedly
//! call `get_output` to read the current output.
//!
//! The chips here are storing values over time, and so we need space to store
//! things within. Accordingly, these chips are actual structs that take up
//! space. In keeping with the project theme of building it all up from as
//! little as possible, each chip is built directly from `DataFlipFlop` values
//! (one single `Wire` value of storage), or from smaller chips from this module
//! that are in turn based on `DataFlipFlop` values.
//!
//! As a mild concession to execution speed, the chips cache their output value
//! after a tick is completed but before the `tick` method returns so that
//! repeated calls to the `get_output` method don't take too long. I think this
//! will be necessary to make the computer simulation debug-mode friendly. I'm
//! not sure, and might change this around later.

pub use arithmetic::*;

use std::mem::uninitialized;

/// A 1-bit register.
#[derive(Debug, PartialEq, Eq, Clone, Default)]
pub struct Bit {
    /// This is where we store the bit.
    data: DataFlipFlop,
}

impl Bit {
    /// If `load` is `One`, the `Bit` will tick into being whatever `input` was.
    /// Otherwise the `Bit` will tick into being it's previous value.
    pub fn tick(&mut self, input: Wire, load: Wire) {
        let old = self.data.get_output();
        self.data.tick(mux(old, input, load));
    }

    /// Obtains the current output of the `Bit`.
    pub fn get_output(&self) -> Wire {
        self.data.get_output()
    }
}

/// A 16-bit register.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Register16 {
    /// The bits of the register.
    bits: [Bit; 16],
}

impl Register16 {
    /// Ticks the register forward. If the load wire is `One` then the `input`
    /// value will go into the register and be returned thereafter. Otherwise
    /// the input is ignored.
    pub fn tick(&mut self, input: [Wire; 16], load: Wire) {
        for i in 0..16 {
            self.bits[i].tick(input[i], load);
        }
    }

    /// Reads the register's current outputs. Be aware that the output isn't
    /// cached directly with individual `Register16` values, so reading this is
    /// more expensive than a strict array copy. You have to read each
    /// individual bit and then build up the output array in a loop.
    pub fn get_output(&self) -> [Wire; 16] {
        unsafe {
            let mut out: [Wire; 16] = uninitialized();
            for i in 0..16 {
                out[i] = self.bits[i].get_output();
            }
            out
        }
    }
}

/// A RAM chip of 8x 16-bit registers.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Ram8x16 {
    /// The memory of the chip
    memory: [Register16; 8],
    /// The cached output value. We have to either store the address during a
    /// tick or the output from the tick at that address, and we choose to store
    /// the output because it's more memory but less computation if `get_output`
    /// is called more than once.
    output: [Wire; 16],
}

impl Ram8x16 {
    /// Ticks the register forward using the specified input wires and the load
    /// wire.
    pub fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 3]) {
        // decide the address that we're working with
        let (load_a, load_b, load_c, load_d, load_e, load_f, load_g, load_h) =
            dmux8way(load, address);
        // tick the individual registers forward
        self.memory[0].tick(input, load_a);
        self.memory[1].tick(input, load_b);
        self.memory[2].tick(input, load_c);
        self.memory[3].tick(input, load_d);
        self.memory[4].tick(input, load_e);
        self.memory[5].tick(input, load_f);
        self.memory[6].tick(input, load_g);
        self.memory[7].tick(input, load_h);
        // update our output cache
        self.output = mux8way16(
            self.memory[0].get_output(),
            self.memory[1].get_output(),
            self.memory[2].get_output(),
            self.memory[3].get_output(),
            self.memory[4].get_output(),
            self.memory[5].get_output(),
            self.memory[6].get_output(),
            self.memory[7].get_output(),
            address,
        );
    }

    /// Reads the register's current outputs.
    pub fn get_output(&self) -> [Wire; 16] {
        self.output
    }
}

/// A RAM chip of 64x 16-bit registers.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Ram64x16 {
    /// The memory of the chip
    memory: [Ram8x16; 8],
    /// The cached output value. We have to either store the address during a
    /// tick or the output from the tick at that address, and we choose to store
    /// the output because it's more memory but less computation if `get_output`
    /// is called more than once.
    output: [Wire; 16],
}

impl Ram64x16 {
    /// Ticks the register forward using the specified input wires and the load
    /// wire.
    pub fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 6]) {
        let super_address = [address[0], address[1], address[2]];
        let sub_address = [address[3], address[4], address[5]];
        // decide where to direct the load signal.
        let (load_a, load_b, load_c, load_d, load_e, load_f, load_g, load_h) =
            dmux8way(load, super_address);
        self.memory[0].tick(input, load_a, sub_address);
        self.memory[1].tick(input, load_b, sub_address);
        self.memory[2].tick(input, load_c, sub_address);
        self.memory[3].tick(input, load_d, sub_address);
        self.memory[4].tick(input, load_e, sub_address);
        self.memory[5].tick(input, load_f, sub_address);
        self.memory[6].tick(input, load_g, sub_address);
        self.memory[7].tick(input, load_h, sub_address);
        // update our output cache
        self.output = mux8way16(
            self.memory[0].get_output(),
            self.memory[1].get_output(),
            self.memory[2].get_output(),
            self.memory[3].get_output(),
            self.memory[4].get_output(),
            self.memory[5].get_output(),
            self.memory[6].get_output(),
            self.memory[7].get_output(),
            super_address,
        );
    }

    /// Reads the register's current outputs.
    pub fn get_output(&self) -> [Wire; 16] {
        self.output
    }
}

/// A RAM chip of 512x 16-bit registers.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Ram512x16 {
    /// The memory of the chip
    memory: [Ram64x16; 8],
    /// The cached output value. We have to either store the address during a
    /// tick or the output from the tick at that address, and we choose to store
    /// the output because it's more memory but less computation if `get_output`
    /// is called more than once.
    output: [Wire; 16],
}

impl Ram512x16 {
    /// Ticks the register forward using the specified input wires and the load
    /// wire.
    pub fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 9]) {
        let super_address = [address[0], address[1], address[2]];
        let sub_address = [
            address[3], address[4], address[5], address[6], address[7], address[8],
        ];
        // decide where to direct the load signal.
        let (load_a, load_b, load_c, load_d, load_e, load_f, load_g, load_h) =
            dmux8way(load, super_address);
        self.memory[0].tick(input, load_a, sub_address);
        self.memory[1].tick(input, load_b, sub_address);
        self.memory[2].tick(input, load_c, sub_address);
        self.memory[3].tick(input, load_d, sub_address);
        self.memory[4].tick(input, load_e, sub_address);
        self.memory[5].tick(input, load_f, sub_address);
        self.memory[6].tick(input, load_g, sub_address);
        self.memory[7].tick(input, load_h, sub_address);
        // update our output cache
        self.output = mux8way16(
            self.memory[0].get_output(),
            self.memory[1].get_output(),
            self.memory[2].get_output(),
            self.memory[3].get_output(),
            self.memory[4].get_output(),
            self.memory[5].get_output(),
            self.memory[6].get_output(),
            self.memory[7].get_output(),
            super_address,
        );
    }

    /// Reads the register's current outputs.
    pub fn get_output(&self) -> [Wire; 16] {
        self.output
    }
}

/// A RAM chip of 4,096x 16-bit registers.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Ram4kx16 {
    /// The memory of the chip
    memory: [Ram512x16; 8],
    /// The cached output value. We have to either store the address during a
    /// tick or the output from the tick at that address, and we choose to store
    /// the output because it's more memory but less computation if `get_output`
    /// is called more than once.
    output: [Wire; 16],
}

impl Ram4kx16 {
    /// Ticks the register forward using the specified input wires and the load
    /// wire.
    pub fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 12]) {
        let super_address = [address[0], address[1], address[2]];
        let sub_address = [
            address[3],
            address[4],
            address[5],
            address[6],
            address[7],
            address[8],
            address[9],
            address[10],
            address[11],
        ];
        // decide where to direct the load signal.
        let (load_a, load_b, load_c, load_d, load_e, load_f, load_g, load_h) =
            dmux8way(load, super_address);
        self.memory[0].tick(input, load_a, sub_address);
        self.memory[1].tick(input, load_b, sub_address);
        self.memory[2].tick(input, load_c, sub_address);
        self.memory[3].tick(input, load_d, sub_address);
        self.memory[4].tick(input, load_e, sub_address);
        self.memory[5].tick(input, load_f, sub_address);
        self.memory[6].tick(input, load_g, sub_address);
        self.memory[7].tick(input, load_h, sub_address);
        // update our output cache
        self.output = mux8way16(
            self.memory[0].get_output(),
            self.memory[1].get_output(),
            self.memory[2].get_output(),
            self.memory[3].get_output(),
            self.memory[4].get_output(),
            self.memory[5].get_output(),
            self.memory[6].get_output(),
            self.memory[7].get_output(),
            super_address,
        );
    }

    /// Reads the register's current outputs.
    pub fn get_output(&self) -> [Wire; 16] {
        self.output
    }
}

/// A RAM chip of 16,384x 16-bit registers.
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct Ram16kx16 {
    /// The memory of the chip
    memory: [Ram4kx16; 4],
    /// The cached output value. We have to either store the address during a
    /// tick or the output from the tick at that address, and we choose to store
    /// the output because it's more memory but less computation if `get_output`
    /// is called more than once.
    output: [Wire; 16],
}

impl Ram16kx16 {
    /// Ticks the register forward using the specified input wires and the load
    /// wire.
    pub fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 14]) {
        let super_address = [address[0], address[1]];
        let sub_address = [
            address[2],
            address[3],
            address[4],
            address[5],
            address[6],
            address[7],
            address[8],
            address[9],
            address[10],
            address[11],
            address[12],
            address[13],
        ];
        // decide where to direct the load signal.
        let (load_a, load_b, load_c, load_d) = dmux4way(load, super_address);
        self.memory[0].tick(input, load_a, sub_address);
        self.memory[1].tick(input, load_b, sub_address);
        self.memory[2].tick(input, load_c, sub_address);
        self.memory[3].tick(input, load_d, sub_address);
        // update our output cache
        self.output = mux4way16(
            self.memory[0].get_output(),
            self.memory[1].get_output(),
            self.memory[2].get_output(),
            self.memory[3].get_output(),
            super_address,
        );
    }

    /// Reads the register's current outputs.
    pub fn get_output(&self) -> [Wire; 16] {
        self.output
    }
}

/// A 16-bit program counter that can be set, incremented, and reset.
#[derive(Debug, PartialEq, Eq, Default, Clone)]
pub struct ProgramCounter16 {
    output: Register16,
}

impl ProgramCounter16 {
    /// A 16-bit program counter ticks as follows:
    ///
    /// * if reset, new_out = 0
    /// * else if load, new_out = in
    /// * else if inc, new_out = old_out + 1
    /// * else new_out = old_out
    pub fn tick(&mut self, input: [Wire; 16], reset: Wire, load: Wire, inc: Wire) {
        // call signature for the register
        // self.output.tick(input, load);
        let old_out = self.output.get_output();
        let accounted_for_inc = mux16(old_out, inc16(old_out), inc);
        let accounted_for_load = mux16(accounted_for_inc, input, load);
        let accounted_for_reset = mux16(accounted_for_load, [Zero; 16], reset);
        self.output.tick(accounted_for_reset, One);
    }

    /// Reads the register's current outputs.
    pub fn get_output(&self) -> [Wire; 16] {
        self.output.get_output()
    }
}
