
//! A module for chips that let you add values in different ways.
//!
//! When you get down to it, basically every kind of math is just some weird
//! form of adding. So with these chips and a little persistance we can build up
//! all the math we want. As with the lower level chips from the `logic` module,
//! these chips have no time element or storage ability, and so they are
//! implemented as simple function calls.
//!
//! The math here uses `[Wire; 16]` arrays to represent 16-bit numbers, but the
//! index of the array and the place value within the number that the arrays
//! represents are **inverted**. This way, when you print out the array, it's
//! close to the same string that you'd get if you printed out the same number
//! in binary format.
//!
//! In other words, the "sign bit" is `array[0]`, the "ones bit" is `array[15]`,
//! and so on.

pub use logic::*;

use std::mem::uninitialized;

/// The value 1, as represented within a `[Wire; 16]`.
#[allow(non_upper_case_globals)]
pub const wire16_one: [Wire; 16] =
    [Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, One];

/// The "sign bit" of a `[Wire; 16]` number.
#[allow(non_upper_case_globals)]
pub const wire16_sign_bit: [Wire; 16] =
    [One, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero, Zero];

/// Adds together just two values, giving a `(sum,carry)` output.
///
/// nand cost: 8
pub fn halfadder(a: Wire, b: Wire) -> (Wire, Wire) {
    (xor(a, b), and(a, b))
}

/// Adds together two values plus a carry value, giving a `(sum,carry)` output.
///
/// nand cost: 28
pub fn fulladder(a: Wire, b: Wire, c: Wire) -> (Wire, Wire) {
    let xorbc = xor(b, c);
    let sum = mux(xorbc, not(xorbc), a);
    let carry = mux(and(b, c), or(b, c), a);
    (sum, carry)
}

/// Adds together two `[Wire; 16]` values.
///
/// Any overflow is ignored.
///
/// nand cost: 448
pub fn add16(a: [Wire; 16], b: [Wire; 16]) -> [Wire; 16] {
    // NOTE: the bit order here is reversed from how you might expect write it.
    // In other words, when writing out a number we write the digits left to
    // right, "123", so that 3 is the "ones" place, 2 is the "tens", etc. But
    // when we convert that directly to an array of numbers we get [1,2,3], and
    // now 1 is at arr[0], 2 is at arr[1], and 3 is at arr[2]. Accordingly,
    // add16 is set up to add the bits from highest index down to lowest.
    unsafe {
        let mut out: [Wire; 16] = uninitialized();
        let mut carry = Zero;
        for i in (0..16).rev() {
            let (sum, carry_this) = fulladder(a[i], b[i], carry);
            out[i] = sum;
            carry = carry_this;
        }
        out
    }
}

/// Increments a `[Wire; 16]` value.
///
/// nand cost: 448
pub fn inc16(input: [Wire; 16]) -> [Wire; 16] {
    add16(input, wire16_one)
}

/// This is the "Arithmetic Logic Unit" of the simulation.
///
/// There are two `[Wire; 16]` inputs, `x` and `y`, which are the primary
/// inputs. There are also a number of control bits that affect the final
/// result:
///
/// * `zx`: zero the `x` bits.
/// * `nx`: negate the `x` bits (after possible zeroing).
/// * `zy`: zero the `y` bits.
/// * `ny`: negate the `y` bits (after possible zeroing).
/// * `f`: function code, 0=`add`, 1=`and`
/// * `no`: negate the output bits.
///
/// So after `zx`, `nx`, `zy`, and `ny` are accounted for, the effective inputs
/// are combined by the function that `f` picks, and then the result might be
/// negated as well (according to `no`).
///
/// There are two additional output bits:
///
/// * The first flag is if the output is zero or not.
/// * The second flag is if the output is negative or not.
///
/// nand cost: 1,426
pub fn alu(x: [Wire; 16], y: [Wire; 16], zx: Wire, nx: Wire, zy: Wire, ny: Wire, f: Wire, no: Wire)
    -> ([Wire; 16], Wire, Wire) {
    let x_zeroed = mux16(x, [Zero; 16], zx);
    let y_zeroed = mux16(y, [Zero; 16], zy);
    let x_actual = mux16(x_zeroed, not16(x_zeroed), nx);
    let y_actual = mux16(y_zeroed, not16(y_zeroed), ny);
    let out_init = mux16(and16(x_actual, y_actual), add16(x_actual, y_actual), f);
    let out = mux16(out_init, not16(out_init), no);
    let out_is_zero = not(or16way(out));
    let out_is_negative = or16way(and16(out, wire16_sign_bit));
    (out, out_is_zero, out_is_negative)
}
