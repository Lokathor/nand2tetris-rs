
//! A module for the most basic of buildable logic gates.
//!
//! Here you can find reprisentations of the basic operations of logic being
//! performed on the `Zero` and `One` values from the `Wire` type in the
//! `primitive` module. The chips here don't have a time element or storage
//! element, and so they are reprisented as functions that you simply call with
//! whatever inputs.
//!
//! Every function here is coded in terms of the `nand` function from the
//! `primitives` module, or in terms of other functions from this same module.
//! Just for fun, each function's documentation includes a "nand cost" value to
//! help you get a sense of how much more intricate some chips are compared to
//! others.

pub use primitives::*;

use std::mem::uninitialized;

/// Accepts one `Wire` and returns the inverse `Wire`.
///
/// [Wikipedia](https://en.wikipedia.org/wiki/Inverter_(logic_gate))
///
/// nand cost: 1
pub fn not(a: Wire) -> Wire {
    nand(a, a)
}

/// Gives `One` if both inputs are `One`, `Zero` otherwise.
///
/// [Wikipedia](https://en.wikipedia.org/wiki/AND_gate)
///
/// nand cost: 2
pub fn and(a: Wire, b: Wire) -> Wire {
    not(nand(a, b))
}

/// Gives `One` if either or both inputs are `One`, `Zero` otherwise.
///
/// [Wikipedia](https://en.wikipedia.org/wiki/OR_gate)
///
/// nand cost: 3
pub fn or(a: Wire, b: Wire) -> Wire {
    nand(not(a), not(b))
}

/// Gives `One` if exactly one of the two inputs is `One`.
///
/// [Wikipedia](https://en.wikipedia.org/wiki/XOR_gate)
///
/// nand cost: 6
pub fn xor(a: Wire, b: Wire) -> Wire {
    and(or(a, b), nand(a, b))
}

/// Gives `a` if the selection wire is `Zero`, and `b` otherwise.
///
/// [Wikipedia](https://en.wikipedia.org/wiki/Multiplexer)
///
/// nand cost: 8
pub fn mux(a: Wire, b: Wire, pick_b: Wire) -> Wire {
    let pick_a = not(pick_b);
    let out_if_pick_a = and(a, pick_a);
    let out_if_pick_b = and(b, pick_b);
    or(out_if_pick_a, out_if_pick_b)
}

/// Passes the input to the left or right output, with the other output being
/// `Zero`.
///
/// [Wikipedia](https://en.wikipedia.org/wiki/Multiplexer#Digital_demultiplexers)
///
/// nand cost: 5
pub fn dmux(input: Wire, send_right: Wire) -> (Wire, Wire) {
    let send_left = not(send_right);
    let left = and(input, send_left);
    let right = and(input, send_right);
    (left, right)
}

/// Performs a `not` on each `Wire` in the provided `[Wire; 16]`.
///
/// nand cost: 16
pub fn not16(inp: [Wire; 16]) -> [Wire; 16] {
    unsafe {
        let mut out: [Wire; 16] = uninitialized();
        for i in 0..16 {
            out[i] = not(inp[i]);
        }
        out
    }
}

/// Performs an `and` operation for each index of the two arrays provided.
///
/// nand cost: 32
pub fn and16(a: [Wire; 16], b: [Wire; 16]) -> [Wire; 16] {
    unsafe {
        let mut out: [Wire; 16] = uninitialized();
        for i in 0..16 {
            out[i] = and(a[i], b[i]);
        }
        out
    }
}

/// Performs an `or` operation for each index of the two arrays provided.
///
/// nand cost: 48
pub fn or16(a: [Wire; 16], b: [Wire; 16]) -> [Wire; 16] {
    unsafe {
        let mut out: [Wire; 16] = uninitialized();
        for i in 0..16 {
            out[i] = or(a[i], b[i]);
        }
        out
    }
}

/// As per `mux`, but for two arrays instead of for two individual values.
///
/// nand cost: 128
pub fn mux16(a: [Wire; 16], b: [Wire; 16], pick_b: Wire) -> [Wire; 16] {
    unsafe {
        let mut out: [Wire; 16] = uninitialized();
        for i in 0..16 {
            out[i] = mux(a[i], b[i], pick_b);
        }
        out
    }
}

/// Uses `or` to merge a `[Wire; 8]` into a single `Wire`.
///
/// nand cost: 24
pub fn or8way(a: [Wire; 8]) -> Wire {
    let w1 = or(a[0], a[1]);
    let w2 = or(w1, a[2]);
    let w3 = or(w2, a[3]);
    let w4 = or(w3, a[4]);
    let w5 = or(w4, a[5]);
    let w6 = or(w5, a[6]);
    or(w6, a[7])
}


/// Uses `or` to merge a `[Wire; 16]` into a single `Wire`.
///
/// This function is not required by the normal course, but it's very useful for
/// building the full ALU chip during the next stage of the project.
///
/// nand cost: 48
pub fn or16way(a: [Wire; 16]) -> Wire {
    let w1 = or(a[0], a[1]);
    let w2 = or(w1, a[2]);
    let w3 = or(w2, a[3]);
    let w4 = or(w3, a[4]);
    let w5 = or(w4, a[5]);
    let w6 = or(w5, a[6]);
    let w7 = or(w6, a[7]);
    let w8 = or(w7, a[8]);
    let w9 = or(w8, a[9]);
    let w10 = or(w9, a[10]);
    let w11 = or(w10, a[11]);
    let w12 = or(w11, a[12]);
    let w13 = or(w12, a[13]);
    let w14 = or(w13, a[14]);
    or(w14, a[15])
}

/// Muxes four different `[Wire; 16]` values according to the two selection
/// wires.
///
/// * sel [Zero, Zero]: a
/// * sel [Zero, One]: b
/// * sel [One, Zero]: c
/// * sel [One, One]: d
///
/// nand cost: 384
pub fn mux4way16(a: [Wire; 16], b: [Wire; 16], c: [Wire; 16], d: [Wire; 16], sel: [Wire; 2])
    -> [Wire; 16] {
    let ab = mux16(a, b, sel[1]);
    let cd = mux16(c, d, sel[1]);
    mux16(ab, cd, sel[0])
}

/// Muxes eight different `[Wire; 16]` values according to the three selection
/// wires.
///
/// * sel [Zero, Zero, Zero]: a
/// * sel [Zero, Zero, One]: b
/// * sel [Zero, One, Zero]: c
/// * sel [Zero, One, One]: d
/// * sel [One, Zero, Zero]: e
/// * sel [One, Zero, One]: f
/// * sel [One, One, Zero]: g
/// * sel [One, One, One]: h
///
/// nand cost: 896
pub fn mux8way16(
    a: [Wire; 16], b: [Wire; 16], c: [Wire; 16], d: [Wire; 16], e: [Wire; 16], f: [Wire; 16],
    g: [Wire; 16], h: [Wire; 16], sel: [Wire; 3]
) -> [Wire; 16] {
    let abcd = mux4way16(a, b, c, d, [sel[1], sel[2]]);
    let efgh = mux4way16(e, f, g, h, [sel[1], sel[2]]);
    mux16(abcd, efgh, sel[0])
}

/// Dmuxes the input four ways according to the two selection values.
///
/// * sel [Zero, Zero]: a
/// * sel [Zero, One]: b
/// * sel [One, Zero]: c
/// * sel [One, One]: d
///
/// nand cost: 15
pub fn dmux4way(inp: Wire, sel: [Wire; 2]) -> (Wire, Wire, Wire, Wire) {
    let (ab, cd) = dmux(inp, sel[0]);
    let (a, b) = dmux(ab, sel[1]);
    let (c, d) = dmux(cd, sel[1]);
    (a, b, c, d)
}

/// Dmuxes the input eight ways according to the three selection values.
///
/// * sel [Zero, Zero, Zero]: a
/// * sel [Zero, Zero, One]: b
/// * sel [Zero, One, Zero]: c
/// * sel [Zero, One, One]: d
/// * sel [One, Zero, Zero]: e
/// * sel [One, Zero, One]: f
/// * sel [One, One, Zero]: g
/// * sel [One, One, One]: h
///
/// nand cost: 35
pub fn dmux8way(input: Wire, sel: [Wire; 3]) -> (Wire, Wire, Wire, Wire, Wire, Wire, Wire, Wire) {
    let (abcd, efgh) = dmux(input, sel[0]);
    let (a, b, c, d) = dmux4way(abcd, [sel[1], sel[2]]);
    let (e, f, g, h) = dmux4way(efgh, [sel[1], sel[2]]);
    (a, b, c, d, e, f, g, h)
}
