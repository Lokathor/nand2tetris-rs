//! Code for the actual computer parts that let you run a fully simulated Hack
//! computer.

use super::*;
pub use sequencing::*;

use std::sync::Mutex;

/// Trait for memory modules that represent a hardware memory map of some form.
///
/// A chip with this trait is combined with a RAM chip into the complete
/// ActiveMemory of the computer. It simulates various parts of the hardware
/// that the computer interfaces with. Things that implement `MemoryMap` are
/// special because they step outside the normal bounds of the simulation and do
/// arbitrary computations and talk to arbitrary IO systems.
pub trait MemoryMap {
    /// Ticks the module forward one step.
    fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 14]);

    /// Gets the module's current output.
    fn get_output(&self) -> [Wire; 16];
}

/// The standard hardware mapping of the Hack VM.
///
/// It provides a Screen and a Keyboard. When combined with a RAM unit into an
/// ActiveMemory they are accessed at the following address of the
/// ActiveMemory:
///
/// * 0x0000 - 0x3FFF: RAM
/// * 0x4000 - 0x5FFF: Screen pixels (1 bit each, starting top left, read/write)
/// * 0x6000: Keyboard scancode (read only)
///
/// Accordingly, when this unit is addressed directly with a 14-bit address, the
/// devices are as follows:
///
/// * 0x0000 - 0x1FFF: Screen
/// * 0x2000: Keyboard scancode (read only)
pub struct StandardMemoryMap {
    screen: Mutex<Vec<u16>>,
    keyboard: Mutex<u16>,
    output: [Wire; 16],
}

impl Default for StandardMemoryMap {
    /// Constructs a default memory map unit.
    fn default() -> Self {
        let screen = Mutex::new(vec![0; 0x2000]);
        let keyboard = Mutex::new(0);
        let output = [Zero; 16];
        StandardMemoryMap {
            screen,
            keyboard,
            output,
        }
    }
}

impl StandardMemoryMap {
    /// Assigns the current scancode of the keyboard.
    pub fn set_keyboard(&self, scancode: u16) {
        (*self.keyboard.lock().unwrap()) = scancode;
    }

    /// Reads out what the screen data is currently set to.
    pub fn read_screen(&self) -> Vec<u16> {
        (*self.screen.lock().unwrap()).clone()
    }
}

impl MemoryMap for StandardMemoryMap {
    /// The rest of the computer _thinks_ that this is a normal chip. Even
    /// though it is not. If the address requested is past the memory safe zone
    /// this will panic.
    fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 14]) {
        let input_u16 = wire16_to_u16(input);
        let address_u16 = wire16_to_u16([
            Zero,
            Zero,
            address[0],
            address[1],
            address[2],
            address[3],
            address[4],
            address[5],
            address[6],
            address[7],
            address[8],
            address[9],
            address[10],
            address[11],
            address[12],
            address[13],
        ]);
        if address_u16 < 0x2000 {
            let index = address_u16 as usize;
            let mut arr_guard = self.screen.lock().unwrap();
            if load == One {
                arr_guard[index] = input_u16;
            };
            self.output = u16_to_wire16(arr_guard[index]);
        } else if address_u16 == 0x2000 {
            // We've selected the keyboard. We set the output to be its scancode.
            let key_guard = self.keyboard.lock().unwrap();
            self.output = u16_to_wire16(*key_guard);
        } else {
            self.output = [Zero; 16];
        };
    }

    /// Reads the current output value of the memory map unit.
    fn get_output(&self) -> [Wire; 16] {
        self.output
    }
}

use std::fmt;
impl fmt::Debug for StandardMemoryMap {
    /// Just says "MemoryMap {}".
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "StandardMemoryMap")
    }
}

/// The computer's mutable memory.
///
/// Includes a RAM chip as well as a memory mapped set of IO devices. The RAM
/// and and the MemoryMap both accept 14-bit addresses. This accepts 15-bit
/// addresses, and the highest bit is used to select between RAM (0) or
/// MemoryMap (1). The remaining bits become the address within the selected
/// device.
#[derive(Debug)]
pub struct ActiveMemory<M: MemoryMap> {
    output: [Wire; 16],
    ram: Ram16kx16,
    hardware: M,
}

impl<M: MemoryMap> ActiveMemory<M> {
    /// Makes a new Memory unit with the given MemoryMap unit.
    pub fn new(hardware: M) -> Self {
        let output = [Zero; 16];
        let ram = Ram16kx16::default();
        ActiveMemory {
            output,
            ram,
            hardware,
        }
    }

    /// Tick the memory forward.
    pub fn tick(&mut self, input: [Wire; 16], load: Wire, address: [Wire; 15]) {
        let (load_ram, load_hardware) = dmux(load, address[0]);
        self.ram.tick(
            input,
            load_ram,
            [
                address[1],
                address[2],
                address[3],
                address[4],
                address[5],
                address[6],
                address[7],
                address[8],
                address[9],
                address[10],
                address[11],
                address[12],
                address[13],
                address[14],
            ],
        );
        self.hardware.tick(
            input,
            load_hardware,
            [
                address[1],
                address[2],
                address[3],
                address[4],
                address[5],
                address[6],
                address[7],
                address[8],
                address[9],
                address[10],
                address[11],
                address[12],
                address[13],
                address[14],
            ],
        );
        // cache our result
        self.output = mux16(
            self.ram.get_output(),
            self.hardware.get_output(),
            address[0],
        );
    }

    /// Reads the Memory's current outputs.
    pub fn get_output(&self) -> [Wire; 16] {
        self.output
    }
}
