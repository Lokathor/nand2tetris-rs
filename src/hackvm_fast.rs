//! A module that allows for simulating a Hack computer in a much faster way
//! than by computing the results of every single little wire and logic gate.

/// A struct for a high-speed Hack VM
#[derive(Debug)]
pub struct HackVM {
    a_register: u16,
    d_register: u16,
    program_counter: u16,
    rom: Vec<u16>,
    ram: Vec<u16>,
}

impl HackVM {
    /// Makes a new HackVM using the given ROM data.
    pub fn new(rom: Vec<u16>) -> Self {
        let a_register = 0;
        let d_register = 0;
        let program_counter = 0;
        let ram = vec![0; 0x8000];
        HackVM {
            a_register,
            d_register,
            program_counter,
            rom,
            ram,
        }
    }

    fn get_instruction(&self) -> u16 {
        *self.rom.get(self.program_counter as usize).unwrap_or(&0)
    }

    /// Tick the computer forward one cycle.
    pub fn tick(&mut self) {
        let instruction = self.get_instruction();
        if is_address_instruction(instruction) {
            // Assign the value and go to the next instruction
            self.a_register = instruction;
            self.program_counter = self.program_counter.wrapping_add(1);
        } else {
            // Invoke the ALU in some way, and then compute a possible jump
            // elsewhere, otherwise we just go to the next instruction.
            let (dest_a, dest_d, dest_m) = get_destination(instruction);
            let jump_type = get_jump(instruction);
            let source = if a_is_pointer(instruction) {
                self.ram[self.a_register as usize]
            } else {
                self.a_register
            };
            let output = alu_fast(self.d_register, source, instruction);
            if dest_a {
                self.a_register = output;
            }
            if dest_d {
                self.d_register = output;
            }
            if dest_m {
                self.ram[self.a_register as usize] = output;
            };
            let output_i = output as i16;
            let do_jump = match jump_type {
                NoJump => false,
                JumpGreater => output_i > 0,
                JumpEqual => output_i == 0,
                JumpGreaterEqual => output_i >= 0,
                JumpLess => output_i < 0,
                JumpNotEqual => output_i != 0,
                JumpLessEqual => output_i <= 0,
                JumpAlways => true,
            };
            if do_jump {
                self.program_counter = self.a_register;
            } else {
                self.program_counter = self.program_counter.wrapping_add(1);
            }
        }
    }

    // TODO: set the keyboard.

    // TODO: get the screen in a sane way.

    /// Lets you look at the RAM within the VM.
    pub fn get_ram(&self) -> &Vec<u16> {
        &self.ram
    }

    /// Inspects the a register
    pub fn get_a_register(&self) -> u16 {
        self.a_register
    }

    /// Inspects the d register
    pub fn get_d_register(&self) -> u16 {
        self.d_register
    }

    /// Inspects the program counter
    pub fn get_program_counter(&self) -> u16 {
        self.program_counter
    }
}

/// An address instruction is any instruction where the highest bit is 0.
fn is_address_instruction(instruction: u16) -> bool {
    (instruction as i16) >= 0
}

#[test]
fn test_is_address_instruction() {
    assert!(is_address_instruction(0b0000000000000000));
    assert!(!is_address_instruction(0b1000000000000000));
}

fn a_is_pointer(instruction: u16) -> bool {
    instruction & 0b0001_0000_0000_0000 > 0
}

fn get_destination(instruction: u16) -> (bool, bool, bool) {
    (
        instruction & 0b0000_0000_0010_0000 > 0,
        instruction & 0b0000_0000_0001_0000 > 0,
        instruction & 0b0000_0000_0000_1000 > 0,
    )
}

enum JumpType {
    NoJump,
    JumpGreater,
    JumpEqual,
    JumpGreaterEqual,
    JumpLess,
    JumpNotEqual,
    JumpLessEqual,
    JumpAlways,
}
use self::JumpType::*;

fn get_jump(instruction: u16) -> JumpType {
    match instruction & 0b0000_0000_0000_0111 {
        0 => NoJump,
        1 => JumpGreater,
        2 => JumpEqual,
        3 => JumpGreaterEqual,
        4 => JumpLess,
        5 => JumpNotEqual,
        6 => JumpLessEqual,
        7 => JumpAlways,
        other => panic!("impossible: {}", other),
    }
}

fn alu_fast(x: u16, y: u16, instruction: u16) -> u16 {
    let zx = instruction & 0b0000_1000_0000_0000 > 0;
    let nx = instruction & 0b0000_0100_0000_0000 > 0;
    let zy = instruction & 0b0000_0010_0000_0000 > 0;
    let ny = instruction & 0b0000_0001_0000_0000 > 0;
    let op = instruction & 0b0000_0000_1000_0000 > 0;
    let no = instruction & 0b0000_0000_0100_0000 > 0;
    let x = if zx { 0 } else { x };
    let x = if nx { !x } else { x };
    let y = if zy { 0 } else { y };
    let y = if ny { !y } else { y };
    let out = if op { x.wrapping_add(y) } else { x & y };
    if no {
        !out
    } else {
        out
    }
}
