#![deny(missing_debug_implementations)]
#![warn(missing_docs)]
#![cfg_attr(feature = "cargo-clippy", allow(clippy::too_many_arguments))]
#![cfg_attr(
    feature = "cargo-clippy",
    allow(clippy::many_single_char_names)
)]
#![cfg_attr(feature = "cargo-clippy", allow(clippy::needless_range_loop))]

//! A library for working with primitive computer parts.
//!
//! You can check out the [website](http://nand2tetris.org/course.php) of the
//! original course if you want to walk through the steps of what's going on
//! here. There's also a [Youtube
//! Playlist](https://www.youtube.com/playlist?list=PLNMIACtpT9BfztU0P92qlw8Gd4vxvvfT1)
//! if you like video lectures.
//!
//! To make things easy, the top level module re-exports everything from all the
//! sub-modules, and also provides some helper functions for easy conversion
//! between data formats.

pub mod primitives;
pub use primitives::*;

pub mod logic;
pub use logic::*;

pub mod arithmetic;
pub use arithmetic::*;

pub mod sequencing;
pub use sequencing::*;

pub mod computer;
pub use computer::*;

pub mod hackvm_fast;
pub use hackvm_fast::*;

use std::mem::uninitialized;

/// Converts a 16-character `&str` of '0' and '1' into a `[Wire; 16]`.
///
/// The first character of the string is the highest bit, so that a string such
/// as "0000000000000011" will be the `[Wire; 16]` for the number 3 within the
/// simulation. This allows normal binary number formatting to create the
/// correct values. It's mostly intended to make unit tests easier to write, but
/// I'm sure you can find other uses for it as well.
///
/// # Panics
///
/// * If the input value is not of the correct length this function will
///   immediately panic.
/// * If any character is a value other than '0' or '1' the function will also
///   panic.
pub fn str16_to_wire16(input: &str) -> [Wire; 16] {
    if input.len() == 16 {
        unsafe {
            let mut out: [Wire; 16] = uninitialized();
            for (i, ch) in input.chars().enumerate() {
                out[i] = match ch {
                    '0' => Zero,
                    '1' => One,
                    _ => panic!("Invalid character: {}", ch),
                }
            }
            out
        }
    } else {
        panic!("invalid input length: {}", input.len());
    }
}

/// Exactly as per `str16_to_wire16`, but for 8-character strings and it
/// produces `[Wire; 8]` values.
pub fn str8_to_wire8(input: &str) -> [Wire; 8] {
    if input.len() == 8 {
        unsafe {
            let mut out: [Wire; 8] = uninitialized();
            for (i, ch) in input.chars().enumerate() {
                out[i] = match ch {
                    '0' => Zero,
                    '1' => One,
                    _ => panic!("Invalid character: {}", ch),
                }
            }
            out
        }
    } else {
        panic!("invalid input length: {}", input.len());
    }
}

/// Converts a `u16` value into a `[Wire; 16]` that will act as the same value
/// within the simulation.
///
/// Note that you can cast `i16` to `u16` and use this function and you'll also
/// get the correct `[Wire; 16]` value.
pub fn u16_to_wire16(mut input: u16) -> [Wire; 16] {
    unsafe {
        let mut out: [Wire; 16] = uninitialized();
        for i in (0..16).rev() {
            out[i] = if input & 1 == 1 { One } else { Zero };
            input >>= 1;
        }
        out
    }
}

/// The inverse of `u16_to_wire16`.
pub fn wire16_to_u16(input: [Wire; 16]) -> u16 {
    let mut output = 0;
    for (i, &wire) in input.iter().enumerate() {
        if wire == One {
            output |= 1;
        };
        if i < 15 {
            output <<= 1;
        };
    }
    output
}
