
//! The core primitives of the hardware simulation.
//!
//! The normal version of the course using HDL provides these parts for you, but
//! here in Rust we've had to implement them ourselves. If you wanted to do this
//! course in Rust yourself you'd start with just this module and then build up
//! from there.

/// A `Wire` value is either `Zero` or `One`. Logic gate functions accept `Wire`
/// values and transmute them into other `Wire` values according to the truth
/// table of the gate in question.
#[derive(PartialEq, Eq, Clone, Copy)]
pub enum Wire {
    /// Low-signal, false, zero, things of that nature.
    Zero,
    /// High-signal, true, one, and so on.
    One,
}
pub use Wire::*;

use std::fmt;
impl fmt::Debug for Wire {
    /// Formats `Zero` as `0` and `One` as `1`.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Zero => write!(f, "0"),
            One => write!(f, "1"),
        }
    }
}

impl From<bool> for Wire {
    /// `true` is `One`, `false` is `Zero`.
    ///
    /// ```rust
    /// use nand2tetris::*;
    /// assert_eq!(One, Wire::from(true));
    /// assert_eq!(Zero, Wire::from(false));
    /// ```
    fn from(val: bool) -> Wire {
        if val { One } else { Zero }
    }
}

impl Default for Wire {
    /// The default `Wire` value is `Zero`
    ///
    /// ```rust
    /// use nand2tetris::*;
    /// assert_eq!(Zero, Wire::default());
    /// ```
    fn default() -> Self {
        Zero
    }
}

/// The Nand Gate is the fundamental logic gate of this project.
///
/// Within the simulation, all other logic gates are defined in terms of this
/// one. The truth table for a Nand gate is as follows:
///
/// | a | b | out |
/// |:-:|:-:|:---:|
/// | 0 | 0 |  1  |
/// | 0 | 1 |  1  |
/// | 1 | 0 |  1  |
/// | 1 | 1 |  0  |
///
/// [Wikipedia](https://en.wikipedia.org/wiki/NAND_gate)
pub fn nand(a: Wire, b: Wire) -> Wire {
    match (a, b) {
        (One, One) => Zero,
        _ => One,
    }
}

/// A `DataFlipFlop` is the fundamental primitive of manipulating a value over
/// time. It stores a single `Wire` value. Use this to build up all the bigger
/// sequential chips.
#[derive(Debug, PartialEq, Eq, Clone, Default)]
pub struct DataFlipFlop {
    /// The stored `Wire` value. Update with `tick` and read with `get_output`.
    data: Wire,
}

impl DataFlipFlop {
    /// Copies the input value directly to the output value.
    pub fn tick(&mut self, input: Wire) {
        self.data = input;
    }

    /// Gets the output value from the DFF.
    pub fn get_output(&self) -> Wire {
        self.data
    }
}
