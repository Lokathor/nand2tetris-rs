[![License](https://img.shields.io/badge/License-CC--BY--SA--3.0-blue.svg)](https://creativecommons.org/licenses/by-sa/3.0/)

# nand2tetris-rs

The [Nand To Tetris](http://nand2tetris.org/) course, done in pure Rust instead
of using the HDL simulator. This is _not_ associated with the official project
in any way.

* [x] Chapter 1: Boolean Logic
* [x] Chapter 2: Boolean Arithmetic
* [x] Chapter 3: Sequential Logic
* [x] Chapter 4: Machine Language
* [x] Chapter 5: Computer Architecture; Needs Tests
* [ ] Chapter 6: Assembler
* [ ] Chapter 7: VM 1 - Stack Arithmetic
* [ ] Chapter 8: VM 2 - Program Control
* [ ] Chapter 9: High-level Language
* [ ] Chapter 10: Compiler 1 - Syntax Analysis
* [ ] Chapter 11: Compiler 2 - Code Generation
* [ ] Chapter 12: Operating System
